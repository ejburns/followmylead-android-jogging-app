package com.example.stepservicetest;

public interface PaceListener {

	/**
	 * Called each time the BPM is updated.
	 */
	public void onBpmUpdate(double value);
	
	/**
	 * Used for passing a value from a service.
	 */
	public void passValue();
	
}
