package com.example.stepservicetest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.ID3v24Frames;
import org.jaudiotagger.tag.id3.ID3v24Tag;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.MediaColumns;

public class SongLibrary {

	private ArrayList<Song> songlist;

	public SongLibrary(Context context, ContentResolver cr) {
		
		makePlaylist(context, cr);
	}

	protected void makePlaylist(Context context, ContentResolver cr) {

		// This refreshes the Song list if this is called. It's not necessary to
		// call this more than once.
		setSonglist(new ArrayList<Song>());
		
		// Checks if the file being referenced is music.		
		String selection = AudioColumns.IS_MUSIC + " != 0";

		String[] projection = {			
				BaseColumns._ID, //ID of the file. Usefulness is questionable.
				MediaColumns.DATA, //A string of the file's location on the device.
				MediaColumns.MIME_TYPE // Gets the MIME type of the file... not using this for checking file extensions because I am not 100% sure about the differences between MP3 and MP4 headers at the present. - Cooper
		};

		Cursor cursor = cr.query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
				projection, selection, null, null);

		while (cursor.moveToNext()) {

			//  Debugging purposes only...
			//  System.out.println(cursor.getPosition());
			//  System.out.println(cursor.getString(0));
			//  System.out.println(cursor.getString(1));
			//  System.out.println(cursor.getString(2));
			  
			//  System.out.println("Type: " + cursor.getString(1).subSequence(
			//  cursor.getString(1).lastIndexOf(".") + 1,
			//  cursor.getString(1).length()) );
			 

			String extension = (String) cursor.getString(1).subSequence(
					cursor.getString(1).lastIndexOf(".") + 1,
					cursor.getString(1).length());

			try {
				AddMP3(cursor.getString(1), extension);
			} catch (CannotReadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TagException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ReadOnlyFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidAudioFrameException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void AddMP3(String filename, String filetype)
			throws CannotReadException, IOException, TagException,
			ReadOnlyFileException, InvalidAudioFrameException {

		// Get the file
		File file = new File(filename);

		if (filetype.equalsIgnoreCase("MP3")) {

			// Read the file -- Check for huge block of green text when
			// debugging
			MP3File mp3file = (MP3File) AudioFileIO.read(file);

			// Check if an ID3v2 tag actually exists

			if (mp3file.hasID3v2Tag()) {

				// Regardless of what ID3v2 tag it is, it will be read as
				// ID3v2.4. Untested against ID3v2.4.
				ID3v24Tag v24tag = mp3file.getID3v2TagAsv24();
				
				//Set i to -11 just in case the song file has no BPM set.
				Integer i = -11;
			
				//If the song does have a BPM, set it.
				if (v24tag.hasFrame(ID3v24Frames.FRAME_ID_BPM)){
					i = Integer.decode(v24tag.getFirst(ID3v24Frames.FRAME_ID_BPM));	
				}
				
				// Get artist and title information.
				String artist = "??";
				String title = "??";
				
				if (v24tag.hasFrame(ID3v24Frames.FRAME_ID_TITLE)){
					title = v24tag.getFirst(ID3v24Frames.FRAME_ID_TITLE);	
				}
				if (v24tag.hasFrame(ID3v24Frames.FRAME_ID_ARTIST)){
					artist = v24tag.getFirst(ID3v24Frames.FRAME_ID_ARTIST);		
				}
				
				
				
				// Make a new Song object...
				Song song = new Song(filename, i.intValue(),
						title, artist, filetype);
				
				//Add album art, if it has any. It's nice but not required. 
				if (v24tag.hasFrame(ID3v24Frames.FRAME_ID_ATTACHED_PICTURE)){
					song.setArt(v24tag.getFirst(ID3v24Frames.FRAME_ID_ATTACHED_PICTURE));		
				}
				
				//Add genre, if possible. It's nice but not required. 
				if (v24tag.hasFrame(ID3v24Frames.FRAME_ID_GENRE)){
					song.setGenre(v24tag.getFirst(ID3v24Frames.FRAME_ID_GENRE));		
				}
				
				// Get Song list and insert it.
				ArrayList<Song> alist = getSonglist();
				alist.add(song);
				setSonglist(alist);
			}

		}

	}

	public ArrayList<Song> getSonglist() {
		return songlist;
	}

	private void setSonglist(ArrayList<Song> songlist) {
		this.songlist = songlist;
	}

}
