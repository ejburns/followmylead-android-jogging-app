package com.example.stepservicetest.model.lastfm.util;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.example.stepservicetest.model.lastfm.artist.Artist;
import com.example.stepservicetest.model.lastfm.auth.Session;

@Root
public class Lfm {
	
	@Attribute
	private String status;
	
	@Element(required=false)
	private Artist artist;
	
	@Element(required=false)
	private Session session;

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
