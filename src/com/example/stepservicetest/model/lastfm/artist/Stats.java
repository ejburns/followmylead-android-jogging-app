package com.example.stepservicetest.model.lastfm.artist;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Stats {

	@Element
	private int listeners;
	
	@Element
	private int playcount;

	public int getListeners() {
		return listeners;
	}

	public void setListeners(int listeners) {
		this.listeners = listeners;
	}

	public int getPlaycount() {
		return playcount;
	}

	public void setPlaycount(int playcount) {
		this.playcount = playcount;
	}
	
	
}
