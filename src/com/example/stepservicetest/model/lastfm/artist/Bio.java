package com.example.stepservicetest.model.lastfm.artist;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root
public class Bio {

	@ElementList(name="links")
	private List<Link> links;
	
	@Element(name="published")
	private String published;
	
	@Element(name="summary", data=true)
	private String summary;
	
	@Element(name="content", data=true)
	private String content;
	
	@Element(name="placeformed", required=false)
	private String placeformed;
	
	@Element(name="yearformed", required=false)
	private String yearformed;
	
	@ElementList(name="formationlist", required=false)
	private List<Formation> formationlist;

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPlaceformed() {
		return placeformed;
	}

	public void setPlaceformed(String placeformed) {
		this.placeformed = placeformed;
	}

	public String getYearformed() {
		return yearformed;
	}

	public void setYearformed(String yearformed) {
		this.yearformed = yearformed;
	}

	public List<Formation> getFormationlist() {
		return formationlist;
	}

	public void setFormationlist(List<Formation> formationlist) {
		this.formationlist = formationlist;
	}
	
}
