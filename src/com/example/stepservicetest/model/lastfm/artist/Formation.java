package com.example.stepservicetest.model.lastfm.artist;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Formation {
	
	@Element(name="yearfrom", required=false)
	private String yearfrom;
	
	@Element(name="yearfrom", required=false)
	private String yearto;

	public String getYearfrom() {
		return yearfrom;
	}

	public void setYearfrom(String yearfrom) {
		this.yearfrom = yearfrom;
	}

	public String getYearto() {
		return yearto;
	}

	public void setYearto(String yearto) {
		this.yearto = yearto;
	}

}
