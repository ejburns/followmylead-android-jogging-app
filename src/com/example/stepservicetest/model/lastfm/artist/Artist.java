package com.example.stepservicetest.model.lastfm.artist;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root
public class Artist {

	@Element(name="name")
	private String name;
	
	@Element(name="mbid", required=false)
	private String mbid;
	
	@ElementList(name="bandmembers", required=false)
	private List<Member> bandmembers;
	
	@Element(name="url")
	private String url;
	
	@ElementList(name="images", inline=true)
	private List<Image> images;
	
	@Element(name="streamable", required=false)
	private int streamable;
	
	@Element(name="ontour", required=false)
	private int ontour;
	
	@Element(name="stats", required=false)
	private Stats stats;
	
	@ElementList(name="similar", required=false)
	private List<Artist> similar;
	
	@ElementList(name="tags", required=false)
	private List<Tag> tags;
	
	@Element(name="bio", required=false)
	private Bio bio;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMbid() {
		return mbid;
	}

	public void setMbid(String mbid) {
		this.mbid = mbid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStreamable() {
		return streamable;
	}

	public void setStreamable(int streamable) {
		this.streamable = streamable;
	}

	public int getOntour() {
		return ontour;
	}

	public void setOntour(int ontour) {
		this.ontour = ontour;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Stats getStats() {
		return stats;
	}

	public void setStats(Stats stats) {
		this.stats = stats;
	}

	public List<Artist> getSimilar() {
		return similar;
	}

	public void setSimilar(List<Artist> similar) {
		this.similar = similar;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public Bio getBio() {
		return bio;
	}

	public void setBio(Bio bio) {
		this.bio = bio;
	}
	
	public List<Member> getBandmembers() {
		return bandmembers;
	}

	public void setBandmembers(List<Member> bandmembers) {
		this.bandmembers = bandmembers;
	}

	public String getAlbumArtUrl() {
		String url = images.get(0).getText();
		for (Image image : images) {
			if (image.getSize().equals("mega")) {
				url = image.getText();
			}
		}
		return url;
	}
	
}
