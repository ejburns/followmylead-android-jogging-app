package com.example.stepservicetest.model.lastfm.artist;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Tag {
	
	@Element
	private String name;
	
	@Element
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
