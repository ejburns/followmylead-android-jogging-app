package com.example.stepservicetest;

public class Song {
	
	private String data;
	private int BPM;
	private String title;
	private String artist;
	private String filetype;
	private String art;
	private String genre;
	
	public Song(String data, int bPM) {
		this.setData(data);
		this.setBPM(bPM);
		
	}
	
	public Song(String data, int bPM, String title, String artist, String filetype) {
		this.setData(data);
		this.setTitle(title);
		this.setBPM(bPM);
		this.setArtist(artist);
		this.setFiletype(filetype);

	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getBPM() {
		return BPM;
	}

	public void setBPM(int bPM) {
		BPM = bPM;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	
	@Override
	public String toString()
	{
		return new String( this.getArtist() + " - " + this.getTitle() + 
				"\nType: " + this.getFiletype() + "BPM: " + this.getBPM() + 
				"\nData: " + this.getData() + 
				"\nArt: " + this.getArt());
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Song))return false;
	    Song otherSong = (Song) other;
	    if (otherSong.data.equals(data)
	    		&& otherSong.BPM == BPM
	    		&& otherSong.title.equals(title)
	    		&& otherSong.artist.equals(artist)
	    		&& otherSong.filetype.equals(filetype)) {
	    	return true;
	    }
	    return false;
	}

	public void setArt(String art) {
		this.art = art;
	}
	
	public String getArt() {
		
		if (this.art != null) {
			return this.art;
		}
		else return null;
		
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
}
