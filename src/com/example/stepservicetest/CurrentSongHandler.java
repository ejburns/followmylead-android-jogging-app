package com.example.stepservicetest;


public class CurrentSongHandler {
	
	public static final int PACE_MSG = 1;
	
	private SongManager songManager;
	private final int MAX_OUT_OF_SYNC = 3;	// Number of times the jogger can be out of sync
	private int timesOutOfSync;				// number of times jogger is out of sync
	private int range;						// A song can play if it is within X BPM of the song playing.
	private boolean firstRun;				// True if it's the first time through
	
	/**
	 * 
	 * @param songlib
	 */
	public CurrentSongHandler(SongLibrary songlib) {
		range = 10;
		timesOutOfSync = 0;
		firstRun = true;
		
		// Set up the song manager
		songManager = new SongManager(songlib);
	}

	/**
	 * Sync the current song with the pace provided.
	 * @param currentPace
	 * @return
	 */
	public boolean syncWithPace(int currentPace) {
		
		boolean songChanged = false;
		
		if (firstRun) {
			songManager.playNextSong(currentPace, range);
			firstRun = false;
			songChanged = true;
		} else {

			Song currentSong = songManager.getCurrentSong();

			// Check to see if the user's pace is out of range
			// and switch songs if it is.
			int high = currentSong.getBPM() + range;
			int low = currentSong.getBPM() - range;

			if (currentPace > high || currentPace < low) {

				if (timesOutOfSync++ >= MAX_OUT_OF_SYNC) {
					songManager.playNextSong(currentPace, range);
					timesOutOfSync = 0;	// reset sync counter.

					songChanged = true;
				}
			}
		}
		
		return songChanged;
	}
	
	/**
	 * Returns the current song
	 * @return
	 */
	public Song getCurrentSong() {
		return songManager.getCurrentSong();
	}
	
	// Release resources
	public void release() {
		songManager.release();
	}
	
}
