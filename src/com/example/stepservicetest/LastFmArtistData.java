package com.example.stepservicetest;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.example.stepservicetest.model.artist.Artist;
import com.example.stepservicetest.model.artist.Lfm;

public class LastFmArtistData {
	
	private static String baseUrl = "http://ws.audioscrobbler.com/2.0/";
	private static String apiKey = "ebe539d232ebfaf1fe991175589ce8b2";
	private static String deviceId = "1234";
	
	public LastFmArtistData() {}
	
	public static Artist getArtistInfo(String artistName) {
		String result = callWebService("?method=artist.getInfo&artist=" + artistName + "&" + getApiKeyString());
		Lfm lfm = new Lfm();
		
		try
        {
            Serializer serializer = new Persister();
            lfm = serializer.read(Lfm.class, result);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		
		
		return lfm.getArtist();
	}
	
	private static String getApiKeyString() {
		return "api_key=" + apiKey;
	}
	
	private static String callWebService(String query){
		String result = "";
		
        HttpClient httpclient = new DefaultHttpClient();  
        HttpGet request = new HttpGet(baseUrl + query);  
        request.addHeader("deviceId", deviceId);
        
        ResponseHandler<String> handler = new BasicResponseHandler();  
        try {  
            result = httpclient.execute(request, handler);  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        httpclient.getConnectionManager().shutdown();
        return result;
    }

}
