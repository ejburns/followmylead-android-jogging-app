package com.example.stepservicetest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

public class PaceUpdater implements SensorEventListener {
	private static final String TAG = "PaceUpdater";
	
	private Timer timer;
	private int timeRange = 10 * 1000;	// time range of 10 seconds
	private ArrayList<AccelData> sensorData;
	
	private List<PaceListener> listeners = new ArrayList<PaceListener>();

	public PaceUpdater() {
		timer = new Timer();
		sensorData = new ArrayList<AccelData>();
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {

		final float alpha = 0.8f;
		float gravity[] = new float[3];
		float linear_acceleration[] = new float[3];

		// Low pass filter (isolate gravity)
		gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
		gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
		gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

		// High pass filter (remove gravity)
		linear_acceleration[0] = event.values[0] - gravity[0];
		linear_acceleration[1] = event.values[1] - gravity[1];
		linear_acceleration[2] = event.values[2] - gravity[2];

		double x = linear_acceleration[0];
		double y = linear_acceleration[1];
		double z = linear_acceleration[2];

		long timestamp = System.currentTimeMillis();
		AccelData data = new AccelData(timestamp, x, y, z);
		sensorData.add(data);

	}

	private double getStepData(){
		// Grab high point in a time range.
		// Disregard data below some threshold
		// Grab local maximum (those are the steps)

		List<AccelData> data = new ArrayList<AccelData>(sensorData);
		
		// Grab the max peak in that time frame
		double max = 0;
		for(AccelData datum : data){
			if(datum.getMagnitude() > max){
				max = datum.getMagnitude();
			}
		}

		// Grab the min valley
		double min = max;
		for(AccelData datum : data){
			if(datum.getMagnitude() < min){
				min = datum.getMagnitude();
			}
		}

		// Made up threshold
		// TODO: Refactor
		double threshold = (max + min) / 2;

		// Disregard data below threshold
		Iterator<AccelData> it = data.iterator();
		while(it.hasNext()){
			if(it.next().getMagnitude() < threshold){
				it.remove();
			}
		}

		//Grab local maxima and pop info into a new ArrayList (for list a,b,c,d,e, if b<c and c>d, c is a max)
		ArrayList<AccelData> steps = new ArrayList<AccelData>();
		for(int i=0; i < data.size() - 3; i++){
			if(data.get(i).getMagnitude() < data.get(i+1).getMagnitude()
					&& data.get(i+1).getMagnitude() > data.get(i+2).getMagnitude()){
				steps.add(data.get(i+1));
			}
		}

		//      Calculate pace data
		double pace = steps.size()*60/(timeRange/1000);

		// Logging
		Log.i("pace", String.valueOf(pace));
		Log.i("steps", steps.size() + " steps in " + timeRange/1000 + " seconds.");
		
		return pace;
	}

	// Every 10 seconds this notifies the listeners that the BPM is updated.
	public void run() {
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				Log.i(TAG, "pace updated");
				
				notifyListeners(getStepData());
				
				sensorData = new ArrayList<AccelData>();
			}
		}, timeRange, timeRange);
	}
	
	/**
	 * Add listeners to the step events.
	 * @param stepListener
	 */
	public void addStepListener(PaceListener stepListener) {
		listeners.add(stepListener);
	}
	
	private void notifyListeners(double bpm) {
		for (PaceListener sl : listeners) {
			sl.onBpmUpdate(bpm);
		}
	}
}
