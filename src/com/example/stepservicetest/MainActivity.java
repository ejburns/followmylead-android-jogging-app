package com.example.stepservicetest;


import java.util.Locale;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.webkit.WebView;
import android.widget.TextView;

import com.example.stepservicetest.PaceService.StepBinder;
import com.example.stepservicetest.model.lastfm.artist.Artist;
import com.example.stepservicetest.model.lastfm.artist.LastFmArtistData;

public class MainActivity extends Activity implements TextToSpeech.OnInitListener {
	private static final String TAG = "MainActivity";
	
	private PaceService paceService;		// the step service
	private boolean bound;					// is the service bound?

	private TextView currentPaceText;		// text view for the current pace
	private TextView currentSongText;		// text view for the current song
	private WebView webView;				// webview for this activity
	
	private TextToSpeech tts;				// Text to speak object
	
	
	/**
	 *  Service connection for binding the service.
	 */
	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			StepBinder sb = (StepBinder) service;
			paceService = sb.getService();
			
			paceService.registerCallback(callback);
			bound = true;
			
			Log.i(TAG, "onServiceConnected");
		}
		
		@Override
		public void onServiceDisconnected(ComponentName className) {
			paceService = null;
			bound = false;
			
			Log.i(TAG, "onServiceDisconnected");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Initialize text to speech
		tts = new TextToSpeech(this, this);
		
		Log.i(TAG, "onCreated");
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		// BIND (START) THE SERVICE
		Intent stepServiceIntent = new Intent(this, PaceService.class);
		bindService(stepServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		// get the text view for the current pace
		currentPaceText = (TextView) findViewById(R.id.current_pace);
		currentSongText = (TextView) findViewById(R.id.song_information);
		webView = (WebView) findViewById(R.id.webview);
	}
	
	@Override 
	protected void onStop() {
		super.onStop();
		
		// Unbind the service
		if(bound) {
			unbindService(serviceConnection);
			bound = false;
		}
		
		// Release the media player
		//currentSongHandler.release();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	//
	// COMMUNICATING WITH THE SERVICE
	//
	
	// Instance of the callback, which is used to define what the service
	//	does with the callback object. In this case, a handler is used
	//	so the service can manipulate the text views.
	private PaceService.ICallback callback = new PaceService.ICallback() {
		
		@Override
		public void loadingLibrary() {
			speak("One moment please. Your song library is being loaded.");
		}
		
		@Override
		public void paceChanged(final double currentPace) {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					updatePaceInfo(currentPace);
				}
			});
		}
		
		@Override
		public void songChanged(final Song currentSong, final double currentPace) {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					updateSongInfo(currentSong);
					audioUpdateInfo(currentSong, currentPace);
				}
			});
		}
	};
	
	// Update pace information
	private void updatePaceInfo(double currentPace) {
		currentPaceText.setText("PACE: " + currentPace);
	}
	
	// Update song / bpm info
	private void updateSongInfo(Song currentSong) {
		Artist artist = LastFmArtistData.getArtistInfo(currentSong.getArtist().replace(" ", "%20"));
		webView.loadUrl(artist.getAlbumArtUrl());
		currentSongText.setText(currentSong.getTitle() 
				+ ", by " + currentSong.getArtist()
				+ " @" + currentSong.getBPM() + "BPM");
	}

	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			
			if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.i("TTS", "This language is not supported.");
			}
			
		} else {
			Log.i("TTS", "Initialization failed.");
		}
	}
	
	/**
	 * Speaks the passed string.
	 * @param textToSpeak
	 */
	private void speak(String textToSpeak) {
		tts.speak(textToSpeak, TextToSpeech.QUEUE_FLUSH, null);
	}
	
	/**
	 * Updates the user with app information via audio.
	 */
	private void audioUpdateInfo(Song currentSong, double currentPace) {
		String textToSpeak = "You are running at a pace of " + currentPace + " steps per minute. ";
		textToSpeak += "The current song is " + currentSong.getTitle() + ", by " + currentSong.getArtist()
				+ " at " + currentSong.getBPM() + " beats per minute.";
		speak(textToSpeak);
	}
}
