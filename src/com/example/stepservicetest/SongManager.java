package com.example.stepservicetest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

public class SongManager {
	
	private static final String TAG = "SongPlayer";
	
	private MediaPlayer mediaPlayer;
	private SongLibrary songLibrary;
	private Song currentSong;

	/**
	 * Constructor
	 * @param songLibrary
	 */
	public SongManager(SongLibrary songLibrary) {
		this.songLibrary = songLibrary;
		
		// Initialize media player
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		
		// Initialize currentSong
		currentSong = new Song("none", 0, "none", "none", "none");
	}
	
	/**
	 * Returns the current song.
	 * @return
	 */
	public Song getCurrentSong() {
		return currentSong;
	}
	
	/**
	 * Plays the next song, given the current pace and range.
	 * @param currentPace
	 * @param range
	 */
	public void playNextSong(int currentPace, int range) {
		int high = currentPace + range;
		int low = currentPace - range;
		List<Song> songs = new ArrayList<Song>();
		
		// Find all the songs within the range
		for (Song song : songLibrary.getSonglist()) {
			if (song.getBPM() < high && song.getBPM() > low && !song.equals(currentSong)) {
				songs.add(song);
			}
		}
		
		// Get the number of possible songs
		int size = songs.size();
		
		// Make sure there is a song there.
		if (size > 0) {
			
			// If there is only one, play it.
			if (size == 1) {
				playSong(songs.get(0));
			} else {
				// Find the song with the closest BPM
				//	to the jogger's pace.
 
				Song next = songs.get(0);
				
				for (Song song : songs) {
					
					int diff = Math.abs(song.getBPM() - currentPace);
					int prevDiff = Integer.MAX_VALUE;
					
					if (diff < prevDiff && !song.equals(currentSong)) {
						next = song;
						diff = prevDiff;
					}
				}
				
				playSong(next);
			}
		} else {
			Random r = new Random();
			playSong(songLibrary.getSonglist().get(r.nextInt(songLibrary.getSonglist().size())));
			// GIVE THE USER A NOTIFICATION
			Log.i(TAG, "NO SONG WITHIN BPM RANGE");
		}
	}
	
	/**
	 * Plays the passed song.
	 * @param song
	 */
	private void playSong(Song song) {
	
		// If the media player is playing, stop it.
		if (mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			//mediaPlayer.release();
		}
		
		// Try to set the next song and play it.
		try {
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setDataSource(song.getData());
			mediaPlayer.prepare();
			mediaPlayer.start();
			
			// Set the currently playing song.
			currentSong = song;
			
		} catch (IOException e) {
			Log.i(TAG, "[Error playing song]: " + e.getMessage());
		}
	}
	
	/**
	 * Release the media player resource.
	 */
	public void release() {
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}	
	}
	
}
