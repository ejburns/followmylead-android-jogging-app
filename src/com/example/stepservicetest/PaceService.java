package com.example.stepservicetest;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class PaceService extends Service {
	private static final String TAG = "StepService";
	
	private SensorManager sensorManager;			// Sensor manager for starting the accelerometer
	private CurrentSongHandler currentSongHandler;	// current song handler
	private SongLibrary songlib;            		// the song library
	
	private double currentPace;						// current pace of the jogger
	
	private PaceUpdater paceDetector;

	@Override
	public void onCreate() {
		Log.i(TAG, "[SERVICE] onCreate");
		
		currentPace = 0;
		
		// Setup stepper
		paceDetector = new PaceUpdater();
		
		// Start the accelerometer
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		
		Sensor accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(paceDetector, accel, SensorManager.SENSOR_DELAY_FASTEST);
        
		// Create the song library.
		songlib = new SongLibrary(this, this.getContentResolver());
        
        // Create current song handler
     	currentSongHandler = new CurrentSongHandler(songlib);
        
		// Add listeners
		paceDetector.addStepListener(stepListener);
		paceDetector.addStepListener(currentSongListener);
		
		// Run the stepper
		paceDetector.run();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	// ---------------------------------------------
	//
	// COMMUNICATING WITH THE ACTIVITY
	//
	private final IBinder binder = new StepBinder();
	
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}
	
	/**
	 * Class for interacting with this service.
	 */
	public class StepBinder extends Binder {
		PaceService getService() {
			return PaceService.this;
		}
	}
	
	// ---------------------------------------------
	// The callback methods will be defined in the activity
	//	and registered with the service. This way, the service
	//	can use the callback object to send updates to the
	//	activity.
	public interface ICallback {
		public void loadingLibrary();
		public void paceChanged(final double currentPace);
		public void songChanged(final Song currentSong, final double currentPace);
	}
	
	private ICallback callback;
	
	public void registerCallback(ICallback cb) {
		callback = cb;
	}

	// ---------------------------------------------
	// Define how the step listener will behave.
	PaceListener stepListener = new PaceListener() {

		@Override
		public void onBpmUpdate(double value) {
			// For each step, increase the number of total
			//	steps taken.
			currentPace = value;
			
			// Pass this value to the activity
			passValue();
		}

		@Override
		public void passValue() {
			if (callback != null)
				callback.paceChanged(currentPace);
		}
	};
	
	
	// ---------------------------------------------
	// Define how the current song listener will behave.
	PaceListener currentSongListener = new PaceListener() {

		@Override
		public void onBpmUpdate(double value) {
			boolean songChanged = currentSongHandler.syncWithPace((int) value);
			if (songChanged && callback != null) {
				callback.songChanged(currentSongHandler.getCurrentSong(), value);
			}
		}

		@Override
		public void passValue() {}
	};
}
